Blender File
------------


**DEPRECATION NOTICE:** This repository has been deprecated. The `blendfile`
module lives on, in [Blender Asset Tracer][BAT].

Just use `from blender_asset_tracer import blendfile`.

[BAT]: https://developer.blender.org/source/blender-asset-tracer/

---------------------------------


Module to inspect a .blend file from Python.
Basic usage can be grasped from the test suit.

Import the module in your script and you are good to go:

   >>> import blendfile

This module supports both Python2.x and Python3.x at the moment.
Python 2.x support may be dropped in the future.

### Test Suit

To run the tests just do:

   >>> pip install tox
   >>> tox
